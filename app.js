/*
 * Quicksort properties:
 *   - Divide and conquer.
 *   - Worst case is O(n^2).
 *   - Average case is O(nlogn) - efficient on average, which is
 *     why it's widely implemented.
 *   - Sorts in place.
 *   -
 */

let x = [2,8,7,1,3,5,6,4];
let n = x.length-1
console.log(quicksort(x,0,n))

function partition(a, p, r) {
    // p is starting index
    // r is ending index  
    
    let x = a[r];       // x is the value at highest index of array
    let i = p - 1;          // index to separate subarrays
    for (let j = p; j < r; j++) {
        console.count();
        if (a[j] < x) {
            i++;
            let temp = a[i];
            a[i] = a[j];
            a[j] = temp;
        }
    }
    let temp = a[i+1];
    a[i+1] = a[r];
    a[r] = temp;
    return i + 1;
}

function quicksort(a,p,r) {
    if (p < r) {
        let q = partition(a,p,r);
        quicksort(a,p,q-1);
        quicksort(a,q+1,r);
    }
    return a;
}

/*
x = [2,5,7,1,3,5,6,4]
quicksort(x,0,7)
    0 < 7
    q = partition(x,0,7)
        x = [2,1,3,7,8,5,6,4]
        returns 2
    quicksort(x,0,1)
        0 < 1
        q = partition(x,0,1)
            x = [1,2,3,7,8,5,6,4]
            returns 0
        quicksort(x,0,-1)
        quicksort(x,1,1)
    quicksort(x,3,7)
        3 < 7
        *
        *
        * 
*/